# Laravel Web Development Kit

Laravel Web Development Kit version 1.01

## Getting Started

This is a web kit for Laravel project up and running on local machine for development and testing purposes.

### Prerequisites

What you need to setup and install

```
Apache 2.4
PHP 7.1
MySQL 5.6
```

## Built With

* [Laravel v5.8](https://laravel.com/docs/5.8) - PHP web framework.
* [React v16.8](https://github.com/facebook/react/releases/tag/v16.8.6) - A JavaScript library for building user interfaces.
* [Bootstrap v4.3](https://getbootstrap.com/docs/4.3/getting-started/introduction/) - Toolkit for developing with HTML, CSS, and JS
* [Font Awesome v4.7](https://fontawesome.com/v4.7.0/) - Vector icons and social logos icon set and toolkit.
## Authors

* **Winston Alcantara** - *Initial work* - [W.](https://www.winstonalcantara.com)
