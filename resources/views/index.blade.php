<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Laravel Web Kit</title>

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet" />
<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet" />

</head>

<body>

<header>
<div class="container">
    <div class="row">
        <!-- Header content here -->
    </div>
</div>
</header>

<main role="main">
<div class="container">
    <div class="row" id="main-body">
        <!-- Main content here -->
    </div>
</div>
</main>

<footer>
    <div class="container">
        <!-- Footer content here -->
    </div>
</footer>

</body>

<!-- Script -->
<script src="{{ asset('js/app.js') }}" rel="stylesheet"></script>
<script src="{{ asset('js/script.js') }}" rel="stylesheet"></script>

</html>
