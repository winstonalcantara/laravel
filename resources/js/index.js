import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

if(document.getElementById('main-body')) {
    ReactDOM.render(
        <App />,
        document.getElementById('main-body')
    );
}
